import { ClockAndDate } from '../components/ClockAndDate';
import { LastRegisters } from '../components/LastRegisters';
import { RegisterTable } from '../components/RegisterTable';

import './Point.scss';

export const Point = () => {
    return (
        <div className="container mt-5">
            <ClockAndDate />
            <LastRegisters />
            <RegisterTable />
        </div>
    );
}