import AWS from 'aws-sdk';
import { ConfigurationOptions } from 'aws-sdk';
import { PointData } from './api.type';
import moment from 'moment';

const tableName = 'PointSystem';

const setConfiguration = () => {
    const configuration: ConfigurationOptions = {
        region: 'us-east-2',
        secretAccessKey: 'Pzi3Y87veOJmwmPuvEKmiwSrWVwrTZktzwhlKoxW',
        accessKeyId: 'AKIASTJUKYFKHDTTBSOC'
    };
    
    AWS.config.update(configuration);
    
    const dynamodb = new AWS.DynamoDB.DocumentClient();

    return dynamodb;
}

export const getData = async () => {
    const dynamodb = setConfiguration();

    const params = {
        TableName: tableName
    }

    const data = await dynamodb.scan(params).promise();

    return data;
}

export const getDataByTodayDate = async () => {
    const dynamodb = setConfiguration();

    const params = {
        TableName: tableName,
        FilterExpression: '#date = :date',
        ExpressionAttributeNames: {
            '#date': 'date'
        },
        ExpressionAttributeValues: {
          ':date': moment().format('DD/MM/YYYY')
        }
    };

    const data = await dynamodb.scan(params).promise();

    return data;
}

export const setData = async (data: PointData)=> {
    let success = true;
    const dynamodb = setConfiguration();
    const dataInfo = await getData();
    const params = {
        TableName: tableName,
        Item: {
            Id: dataInfo.Count,
            date: data.date,
            time: data.time
        }
    }
    
    dynamodb.put(params, function (err) {
        if(err){
            success = false;
        }
    })

    return success;
}