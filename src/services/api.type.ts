export type PointData = {
    id?: string;
    date: string;
    time: string;
}