import { useEffect, useState } from 'react';
import './RegisterTable.scss';
import { getData } from '../services/api';
import _ from 'lodash';
import moment from 'moment';
import { DataInitialStructure, Mark, StructuredItems } from './RegisterTable.type';

export const RegisterTable = () =>{
    const [structuredItems, setStructuredItems] = useState<StructuredItems[]>([]);

    useEffect(() => {
        let data: DataInitialStructure[] = [];
        let structuredItemsAux: StructuredItems[] = [];
        const defaultDate = new Date('2000-01-01')

        getData().then((result) => {
            result.Items?.map((item) => {
                data = [...data, 
                    {
                        id: item.Id, 
                        date: moment(item.date, 'DD/MM/YYYY').toDate(), 
                        dateTime: moment(`${item.date} ${item.time}`, 'DD/MM/YYYY HH:mm').toDate()
                    }
                ];
            });

            const sortedByDateItems = _.orderBy(data, 'date', 'desc');
            const sortedByDateTimeItems = _.mapValues(_.groupBy(sortedByDateItems, 'date'), item => _.orderBy(item, 'dateTime', 'asc'));

            console.log('Sorted Items', sortedByDateTimeItems);

            Object.entries(sortedByDateTimeItems).map(([date, items]) => {
                let markings: Mark[] = [];
                let displayedMarking = '';
                let totalHours = 0;
                let totalMinutes = 0;
                let dateAux: Date = defaultDate;

                items.map((item, index) => {
                    markings = [ ...markings, { id: Number(item.id), time: item.dateTime } ];

                    if (index % 2 == 0) {
                        displayedMarking = displayedMarking.concat(moment(item.dateTime).format('HH:mm'));
                    } else {
                        displayedMarking = displayedMarking.concat(`-${moment(item.dateTime).format('HH:mm')}  `);
                    }

                    if (dateAux == defaultDate) {
                        dateAux = item.dateTime;
                    } else {
                        const diff = item.dateTime.getTime() - dateAux.getTime();
                        const diffHour = Math.floor(diff / (1000 * 60 * 60));
                        const diffMinutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));

                        if ((totalMinutes + diffMinutes) > 60) {
                            const hours = Math.floor((totalMinutes + diffMinutes) / 60);
                            const minutes = Math.floor((totalMinutes + diffMinutes) - (hours * 60));

                            totalHours += hours;
                            totalMinutes = minutes;
                        }else{
                            totalMinutes += diffMinutes;
                        }

                        totalHours += diffHour;

                        dateAux = defaultDate;
                    }
                });

                const formatedDate = moment(date).format('DD/MM/YYYY');

                structuredItemsAux = [ ...structuredItemsAux, 
                    { 
                        date: formatedDate, 
                        displayedMarking: displayedMarking, 
                        markings: markings, 
                        total: moment(`${formatedDate} ${totalHours}:${totalMinutes}`, 'DD/MM/YYYY HH:mm').format('HH:mm')
                    } 
                ];
            });
                
            setStructuredItems(structuredItemsAux);

            console.log('structuredItemsAux: ', structuredItemsAux)
        });
    }, []);

    return (
        <table className='table table-dark table-striped'>
            <thead>
                <tr>
                    <th scope='col'>Dia</th>
                    <th scope='col'>Marcações</th>
                    <th scope='col'>Total</th>
                </tr>
            </thead>
            <tbody>
                {
                    structuredItems && structuredItems.map((item, index) => {
                        return <tr key={index}>
                            <td>{item.date}</td>
                            <td>{item.displayedMarking}</td>
                            <td>{item.total}</td>
                        </tr>
                    })
                }
            </tbody>
        </table>
    )
}