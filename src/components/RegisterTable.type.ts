export type Mark = {
    id: number,
    time: Date
}

export type StructuredItems = {
    date: string,
    displayedMarking: string,
    markings: Mark[],
    total: string
}

export type DataInitialStructure = {
    id: string,
    date: Date,
    dateTime: Date
}