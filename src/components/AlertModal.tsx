import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import './AlertModal.scss';

type Props = {
  message: string;
  isOpen: boolean;
  onClose: () => void;
}

export const AlertModal = (args: Props) => {
  const toggle = () => args.onClose();

  return (
    <div>
      <Modal isOpen={args.isOpen} toggle={toggle} size='sm'>
        <ModalHeader>
          
        </ModalHeader>
        <ModalBody>
          {args.message}
        </ModalBody>
        <ModalFooter className='justify-content-center'>
          <Button color='secondary' onClick={toggle}>
            Ok
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}