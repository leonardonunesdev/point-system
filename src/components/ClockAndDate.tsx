import { useEffect, useState } from 'react';
import moment from 'moment';
import { setData } from '../services/api';

import './ClockAndDate.scss'
import { Button, Form, Spinner } from 'reactstrap';
import { AlertModal } from './AlertModal';

export const ClockAndDate = () => {
    const [timer, setTimer] = useState<NodeJS.Timer>();
    const [date, setDate] = useState(moment());
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [modalMessage, setModalMessage] = useState("");
    const [isSubmitting, setIsSubmitting] = useState(false);

    const refreshclock = () => {
        setDate(moment());
    }

    useEffect(() => {
        setTimer(setInterval(refreshclock, 1000));

        return function cleanup() {
            clearInterval(timer);
        }
    }, []);

    useEffect(() => {
        if(!isModalOpen){
            setIsSubmitting(false)
        }
    }, [isModalOpen])

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
        setIsSubmitting(true);
        event.preventDefault();

        const success = await setData({
            date: date.format('DD/MM/YYYY'),
            time: date.format('HH:mm')
        });

        setModalMessage(success ? 'Registro realizado com sucesso' : 'Error ao realizar o registro');
        setIsModalOpen(true);
    }

    const closeModal = () => {
        setIsModalOpen(false);
    };

    return (
        <div className='clock-and-date mb-5'>
            <Form onSubmit={handleSubmit}>
                <div className='clock'>{date.format('HH:mm')}</div>
                <div className='date'>{date.format('DD/MM/YYYY')}</div>
                <Button type='submit' className='mt-4' color='secondary'>
                    {isSubmitting ? <Spinner color='light' size='sm'/> : 'Registrar'}
                </Button>
            </Form>
            {modalMessage && <AlertModal message={modalMessage} isOpen={isModalOpen} onClose={closeModal}/>}
        </div>
    );
}