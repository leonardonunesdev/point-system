import { useEffect, useState } from 'react';
import './LastRegisters.scss';
import { getDataByTodayDate } from '../services/api';
import _ from 'lodash';
import moment from 'moment';

export const LastRegisters = () => {
    const [data, setData] = useState<Date[]>([]);

    useEffect(() => {
        let items: Date[] = [];
    
        getDataByTodayDate().then((result) => {
            result.Items?.map((item) => {
                items = [...items, moment(`${item.date} ${item.time}`, 'DD/MM/YYYY HH:mm').toDate()];
            });
    
            items = _.orderBy(items);
    
            setData(items);
        });
    }, [])

    return (
        <div className='last-registers'>
            <span>Últimos registros</span>
            <ul>
                {
                    data.length > 0 && data.map((item, index) => {
                        console.log(item)

                        return <li key={index}>{moment(item.toString()).format('HH:mm')}</li>
                    })
                }
            </ul>
        </div>
    );
}